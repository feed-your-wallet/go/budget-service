package repository

import (
	. "budget_service/model"
	"budget_service/util/database"
)

func AddBudget(b *Budget) {
	database.DBConn.Create(b)
}
