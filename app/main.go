package main

import (
	. "budget_service/dto"
	budgetService "budget_service/service"
	"budget_service/util/database"
)

func main() {
	database.InitConnection()
	database.AutoMigrate()

	budgetDto := NewBudgetDto([]uint{1, 2, 3}, 15, []uint{2, 5, 8}, "My budget", 1)

	budgetService.AddBudget(&budgetDto)
}
