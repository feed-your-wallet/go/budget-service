package dto

type BudgetDto struct {
	ID          uint
	Version     uint
	AccountIds  []uint
	Amount      float64
	CategoryIds []uint
	Name        string
	Period      uint
}

func NewBudgetDto(accountIds []uint, amount float64, categoryIds []uint, name string, period uint) BudgetDto {
	return BudgetDto{
		Version:     1,
		AccountIds:  accountIds,
		Amount:      amount,
		CategoryIds: categoryIds,
		Name:        name,
		Period:      period,
	}
}
