package service

import (
	. "budget_service/dto"
	"budget_service/repository"
	"budget_service/util/mapper"
)

func AddBudget(b *BudgetDto) {
	entity := mapper.ToBudgetEntity(b)
	repository.AddBudget(&entity)
}
