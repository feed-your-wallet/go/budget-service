package database

import (
	"budget_service/model"
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "postgres"
	dbName   = "budgets"
	sslMode  = "disable"
	timeZone = "Europe/London"
)

var (
	DBConn *gorm.DB
)

func InitConnection() {
	dsn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=%s TimeZone=%s",
		host, port, user, password, dbName, sslMode, timeZone)
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	DBConn = db

	if err != nil {
		panic("Failed to connect to database")
	}

	fmt.Println("Database successfully opened")
}

func AutoMigrate() {
	err := DBConn.AutoMigrate(model.Budget{})
	if err != nil {
		panic("Failed to auto-migrate")
	}
}
