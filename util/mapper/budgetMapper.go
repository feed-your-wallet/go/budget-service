package mapper

import (
	. "budget_service/dto"
	. "budget_service/model"
	"github.com/lib/pq"
)

func ToBudgetEntity(dto *BudgetDto) Budget {
	accountIds := pq.Int64Array{}
	categoryIds := pq.Int64Array{}
	for _, accountId := range dto.AccountIds {
		accountIds = append(accountIds, int64(accountId))
	}
	for _, categoryId := range dto.CategoryIds {
		categoryIds = append(categoryIds, int64(categoryId))
	}
	return NewBudget(accountIds, dto.Amount, categoryIds, dto.Name, dto.Period)
}
