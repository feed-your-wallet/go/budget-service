package model

import (
	"github.com/lib/pq"
	"gorm.io/gorm"
)

type Budget struct {
	gorm.Model
	Version     uint
	AccountIds  pq.Int64Array `gorm:"type:integer[]"`
	Amount      float64
	CategoryIds pq.Int64Array `gorm:"type:integer[]"`
	Name        string
	Period      uint
}

func NewBudget(accountIds pq.Int64Array, amount float64, categoryIds pq.Int64Array, name string, period uint) Budget {
	return Budget{
		Version:     1,
		AccountIds:  accountIds,
		Amount:      amount,
		CategoryIds: categoryIds,
		Name:        name,
		Period:      period,
	}
}
